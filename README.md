Live link:	http://35.244.26.134:5000/index.html

Technology stacks:
	- Backbone.js
	- Underscore.js
	- Node.js

Git developer API "https://api.github.com"

Porject Explanation:
	Its a very simple UI to list all the opened issues of any public git repository and its other details.
	- first user have to enter a valide public git repositroy path like: "expressjs/express"
	- then "https://api.github.com/repos/<repo_path>" url will be hit to get repository related data.
	- after that "https://api.github.com/repos/<repo_path>/issues?page=1&per_page=50" will be hit with pagination (only 50 entries at a time).
	- when user scroll down then next 50 entries will be fetch and display.
	
Improvement:
	- We can also show the comments related to each issue.
	- commenter name, likes, dislikes and replies etc.
	- edit option to repository related details.
	- we can also provide options to comment, reply and create a new issue and lots of other things.
	