var http = require('http');
var fs = require('fs');

http.createServer((req, res)=>{
	var file_name = req.url.replace('/', "");
	fs.readFile(file_name, (err, result)=>{
		res.end(result);
	});
}).listen(8080,() => {
	console.log("server started on port 8080");
});