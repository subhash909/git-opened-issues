var GitOpenedIssue = Backbone.View.extend({
	
	// bind events
	events: {
		"click #git_repo_form_submit": "git_repo_form_submit"
	},

	initialize: function(){
		_.bindAll(this, "window_scrolled");
		this.page_count = 1;
		this.repo_url = "";
		$(window).scroll(this.window_scrolled);
	},

	// render the main template
	render: function(){
		$(this.el).html(this.template());
		return this;
	},

	// Handle form submit event
	git_repo_form_submit: function(events){
		var repo_url = $(this.el).find('#git_repo_form').find('#git_repo_url').val();
		if(!repo_url) return false;
		this.repo_url = repo_url;
		this.page_count = 1;
		$(this.el).find('#repo_opened_issue_table_body').html("");
		$(this.el).find('#repo_opened_issue_count').text("");
		this.fetch_repository_details();
	},


	// Populate the fetched result into table
	populate_opened_issue_in_table: function(opened_issues){
		var opened_last_24 = "";
		var opened_last_24_7 = "";
		var opened_last_more_7 = "";
		var total_opened_issues = 0;
		if(opened_issues){
			for(tmp_index in opened_issues){
				try{
					var tmp_opened_issue = opened_issues[tmp_index] || {};
					if(tmp_opened_issue.closed_at==null && tmp_opened_issue.state=="open"){
						total_opened_issues += 1;
						var create_date = tmp_opened_issue.created_at ? new Date(tmp_opened_issue.created_at) : new Date();
						var tmp_html = `<tr><td>${tmp_opened_issue.title}</td><td>${create_date}</td><td>TMP_CATEGORY</td></tr>`;
						var time_diff = (new Date().getTime() - create_date.getTime()) / (1000*60*60*24);
						if(time_diff<=1){
							opened_last_24 += tmp_html.replace("TMP_CATEGORY", "Opened in the last 24 hours");
						}else if(time_diff<=7){
							opened_last_24_7 += tmp_html.replace("TMP_CATEGORY", "Opened more than 24 hours ago but less than 7 days ago");
						}else{
							opened_last_more_7 += tmp_html.replace("TMP_CATEGORY", "Opened more than 7 days ago");
						}
					}
				}catch(err){
					console.log("Error occurred in populate_opened_issue_in_table: ", err);
				}
			}
			var final_html = `${opened_last_24}${opened_last_24_7}${opened_last_more_7}`;
			$(this.el).find('#repo_opened_issue_table_body').append(final_html);
			$(this.el).find('#repo_result').show();
		}
	},

	// Fetch repository details
	fetch_repository_details: function(){
		var that = this;
		var repo_full_url = `https://api.github.com/repos/${that.repo_url}`;
		$.ajax({
			url: repo_full_url,
			type: 'GET',
			success: function(result){
				var issue_count = (result.has_issues && result.open_issues_count) || 0;
				$(that.el).find('#repo_opened_issue_count').text(`Total opened issues: ${issue_count}`);
				if(issue_count>0){
					that.page_count = 1;
					that.fetch_git_opened_issues();
				}
			},
			error: function(err){
				console.log(err);
				$(that.el).find("#repo_result").show();
				$(that.el).find("#repo_opened_issue_count").text("Please provide a valid public git repository.");
			}
		})
	},

	// Fetch git repository opened issues
	fetch_git_opened_issues: function(){
		var that = this;
		var repo_full_url = `https://api.github.com/repos/${that.repo_url}/issues?page=${that.page_count}&per_page=50`;
		$.ajax({
			url: repo_full_url,
			type: 'GET',
			success: function(result){
				that.populate_opened_issue_in_table(result);
			},
			error: function(err){
				console.log(err);
				$(that.el).find("#repo_result").show();
				$(that.el).find("#repo_opened_issue_count").text("Please provide a valid public git repository.");
			}
		})
	},

	window_scrolled: function(event){
		var that = this;
		var x1 = $(document).scrollTop();
		var x2 = $('body').prop('clientHeight');
		var y = $('body').prop('scrollHeight')
		if(x1 + x2 == y)
		{
			this.page_count += 1;
			this.fetch_git_opened_issues();
		}
	},

	// Main template
	template: _.template(''+
		'<div>'+
			'<div id="git_repo_form">'+
				'<form class="form-horizontal">'+
					'<div class="form-group">'+
						'<label for="git_repo_url">Git Respository:</label>'+
						'<input type="text" class="form-control" id="git_repo_url" placeholder="provide a public GitHub repository like: <:owner>/<:repo>">'+
					'</div>'+
					'<div class="form-group">'+
						'<button type="button" id="git_repo_form_submit" class="btn btn-primary">Submit</button>'+
					'</div>'+
				'</form>'+
			'</div>'+
			'<hr/>'+
			'<div id="repo_result" style="display:none;">'+
				'<b><h3 id="repo_opened_issue_count"></h3></b>'+
				'<table class="table table-bordered" id="repo_opened_issue_table">'+
					'<thead>'+
						'<tr>'+
							'<th>Title</th>'+
							'<th>Opened Date</th>'+
							'<th>Category</th>'+
						'</tr>'+
					'</thead>'+
					'<tbody id="repo_opened_issue_table_body">'+
					'</tbody>'+
				'</thead>'+
			'</div>'+
		'</div>'+
	'')
});

// View rendering
new GitOpenedIssue({el: $("#body")}).render();